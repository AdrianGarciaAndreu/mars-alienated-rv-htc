using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class appear : MonoBehaviour
{
    // UI Canvas
    public GameObject infoPrefab;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
      
    }

    // Function witch creat the canvas UI
    public void info_appear()
    {
        Destroy(GameObject.Find(infoPrefab.name + "(Clone)"));
        GameObject info = Instantiate(infoPrefab);
        info.transform.position = new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y, this.gameObject.transform.position.z);
    }
}
