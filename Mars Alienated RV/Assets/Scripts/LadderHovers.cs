using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LadderHovers : MonoBehaviour
{
    private void OnTriggerStay(Collider other)
    {
        setbGrip(other.transform.parent, true);
    }
    private void OnTriggerExit(Collider other)
    {
        setbGrip(other.transform.parent, false);
    }
    private void setbGrip(Transform parent1, bool bGrip)
    {
        if (parent1 != null)
        {
            Transform parent2 = parent1.parent;
            if (parent2 != null)
            {
                if (parent2.name.Contains("HandColliderRight"))
                {
                    parent2.parent.GetComponent<ClimbingAction>().bInGrip[1] = bGrip;
                }
                else if (parent2.name.Contains("HandColliderLeft"))
                {
                    parent2.parent.GetComponent<ClimbingAction>().bInGrip[0] = bGrip;
                }
            }
        }
    }
}
