using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoomToObject : MonoBehaviour
{
    private bool isZoomed = false;

    private Vector3 oldPlayerPosition;
    private Quaternion oldCameraRotation;

    public GameObject player;
    public GameObject camera;
    public GameObject arrows;
    public Vector3 newPlayerPosition;
    public Vector3 newCameraRotation;
    public GameObject zoomCanvas;

    public void setZoomToCamera()
    {
        GameObject.Find("Tutorial").GetComponent<Tutorial>().startExitZoomState();

        if (!isZoomed)
        {
            oldPlayerPosition = player.transform.localPosition;
            oldCameraRotation = camera.transform.localRotation;
            Debug.Log(oldPlayerPosition);


            player.transform.position = newPlayerPosition;
            camera.transform.localRotation = Quaternion.Euler(newCameraRotation);

            this.gameObject.GetComponent<BoxCollider>().enabled = false;
            isZoomed = true;
            zoomCanvas.SetActive(true);
            arrows.SetActive(false);
        }
    }

    public void setOriginalCameraPosition()
    {
        player.transform.position = oldPlayerPosition;
        camera.transform.localRotation = oldCameraRotation;

        this.gameObject.GetComponent<BoxCollider>().enabled = true;
        isZoomed = false;
        zoomCanvas.SetActive(false);
        arrows.SetActive(true);
    }

    private void Update()
    {
        if (Input.GetButtonDown("B") && this.isZoomed)
        {
            GameObject.Find("Tutorial").GetComponent<Tutorial>().startFoundIpadState();

            setOriginalCameraPosition();
        }
    }
}
