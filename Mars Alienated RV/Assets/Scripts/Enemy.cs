using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{

    public float maxDuration = 10f;

    private EnemyManager em;

    private float currentTime = 0f;

    // Start is called before the first frame update
    void Start()
    {
        em = GameObject.Find("VRCamera").GetComponent<EnemyManager>();
        StartCoroutine(customLoop());
    }


    private IEnumerator customLoop()
    {
        while (true) {
            if (currentTime >= maxDuration) { break; }
            currentTime++;
            yield return new WaitForSeconds(1.0f); // Each second
        }

        em.SetEnemyInGame(false);
        Destroy(this.gameObject);
    }

    public void delayedDestroy(float seconds){
        em.SetEnemyInGame(false);
        Destroy(this.gameObject, seconds);
    }


}
