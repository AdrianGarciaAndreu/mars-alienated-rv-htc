using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenFinalDoor : MonoBehaviour
{
    public OpenCloseDoor door;

    private GameManager gm;
    private bool isDoorOpen = false;
    // Start is called before the first frame update
    void Start()
    {
        this.gm = GameManager.Instance;
    }

    // Update is called once per frame
    void Update()
    {
        if (!isDoorOpen)
        {
            if(gm.totalGatheredData == 100)
            {
                isDoorOpen = true;
                door.openDoor();
            }
        }
    }
}
