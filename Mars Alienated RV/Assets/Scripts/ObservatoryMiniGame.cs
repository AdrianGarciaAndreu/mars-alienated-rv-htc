using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObservatoryMiniGame : MonoBehaviour
{
    public List<GameObject> cranks = new List<GameObject>();
    public List<float> correctAngleY = new List<float>();
    //public OpenCloseDoor door;

    private int correctCranks = 0;
    private bool isFirstTime = true;

    private GameManager gm;

    void Start()
    {
        this.gm = GameManager.Instance;
    }

    // Update is called once per frame
    void Update()
    {
        if (isFirstTime)
        {

            correctCranks = 0;

            for (int i = 0; i < cranks.Count; i++)
            {
                float zPosition = cranks[i].transform.localEulerAngles.z;

                Debug.Log(zPosition);

                if ((zPosition > (correctAngleY[i] - 10)) && (zPosition < (correctAngleY[i] + 10)))
                {
                    correctCranks++;
                }
            }

            if (correctCranks == cranks.Count)
            {
                gm.totalGatheredData += 25;
                isFirstTime = false;
                //door.openDoor();
            }
        }
    }
}
