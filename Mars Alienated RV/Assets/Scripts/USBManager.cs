using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class USBManager : MonoBehaviour
{

    private GameManager gm;
    public int dataToAdd = 25;

    public void gatherUSB(){
        gm = GameManager.Instance;
        gm.totalGatheredData += dataToAdd;

        Destroy(this.gameObject);
    }

}
