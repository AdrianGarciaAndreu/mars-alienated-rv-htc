using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class TimeManager : MonoBehaviour
{
    GameManager gm;
    private int totalGameTime;
    public int currentGameTime;

    private string mm, ss; // Holds time in mm:ss format

    // Start is called before the first frame update
    void Start(){
        
        // load GameManager and Game Time 
        this.gm = GameManager.Instance; 
        this.totalGameTime = this.gm.totalGameTime;

        this.currentGameTime = this.totalGameTime;

        StartCoroutine(timeLoop());
    }


    /*
     * passes from secons to MM SS Format
     */
    private void secondsToMMSSFormat(int timeInSeconds){

        if (timeInSeconds>59) { // 1 minute or mroe
            int isolatedMinutes = (int)(timeInSeconds / 60);
            int isolatedSeconds = (int)(timeInSeconds % 60);

            this.mm = string.Format("{00}:", isolatedMinutes);
            if (isolatedMinutes < 10) { this.mm = "0" + this.mm; }

            this.ss = string.Format("{00}", isolatedSeconds.ToString());
            if (isolatedSeconds < 10) { this.ss = "0" + this.ss; }
            
        }
        else{ // just remains seconds!!
            
            this.mm= "00:";
            this.ss = string.Format("{00}", timeInSeconds.ToString());
            if (timeInSeconds < 10) { this.ss = "0" + this.ss; }

        }


    }

    /*
     * Updates the text on digital timer
     */
    private void updateGameTimer(){

        TextMeshPro tmp = this.gameObject.GetComponent<TextMeshPro>();
        tmp.SetText(this.mm+this.ss);
   
    }


    private IEnumerator timeLoop(){

        while (true){

            if (this.currentGameTime > 0) { 

                // removes 1 second from game time
                this.currentGameTime--;
            
                secondsToMMSSFormat(this.currentGameTime);
                updateGameTimer();
            
                yield return new WaitForSeconds(1f); // each second
            }
            else{
                // Player Lost (Time ended)
                this.gm.gameLost = true; this.gm.gameOver = true;
                Debug.Log("Definitive Game OVER!");
                break; // Ends loop
            }
        } // End While loop

    }




}
