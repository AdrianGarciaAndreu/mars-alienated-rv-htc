using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpElevator : MonoBehaviour
{
    public Animator animator;
    public AudioSource audioSource;

    private bool isElevatorUp = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!isElevatorUp)
        {
            isElevatorUp = true;
            audioSource.Play();
            animator.SetTrigger("upElevator");
        }
    }
}
