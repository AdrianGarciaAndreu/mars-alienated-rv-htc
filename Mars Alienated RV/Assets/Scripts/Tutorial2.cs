using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tutorial2 : MonoBehaviour
{
    public List<Sprite> images = new List<Sprite>();
    public Image imgObject;
    public GameObject rightControl;
    public GameObject leftControl;

    private int index = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void changeImage(string direction)
    {
        if (direction == "right")
        {
            if (index != images.Count)
            {
                index++;

                imgObject.sprite = images[index];

                if(index == images.Count-1)
                {
                    rightControl.SetActive(false);
                    leftControl.SetActive(true);
                }
            }
        }
        else
        {
            if (index != 0)
            {
                index--;

                imgObject.sprite = images[index];

                if (index == 0)
                {
                    leftControl.SetActive(false);
                    rightControl.SetActive(true);
                }
            }
        }

        if(index != 0 && index != (images.Count-1))
        {
            leftControl.SetActive(true);
            rightControl.SetActive(true);
        }
    }
}
