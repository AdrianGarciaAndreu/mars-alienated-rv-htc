using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFinalSpawner : MonoBehaviour
{
    public GameObject EnemyToInstantiate;
    public List<Light> lightsToSwitchOff = new List<Light>();
    public List<AudioSource> lightsSound = new List<AudioSource>();

    public List<GameObject> lightsToSwitchFlishlight = new List<GameObject>();

    private bool isEnemyShowed = false;

    public GameObject player;

    private void OnTriggerEnter(Collider other){

        if(other.gameObject.tag == "Player") {
            if (!isEnemyShowed)
            {
                GameManager gm = GameManager.Instance;
                gm.isInScapeRoom = true;

                EnemyToInstantiate.SetActive(true);
                StartCoroutine(switchOffLights());
                switchOnFlashlight();
                changePlayerPhysics();
            }
        }
        
    }

    private void changePlayerPhysics()
    {
        player.GetComponent<Rigidbody>().isKinematic = true;
        player.GetComponent<Rigidbody>().freezeRotation = false;
        player.GetComponent<BoxCollider>().enabled = false;
    }

    IEnumerator switchOffLights()
    {
        int count = 0;
        float time = 1;
        foreach (Light light in lightsToSwitchOff)
        {
            lightsSound[count].Play();
            count++;

            light.range = 100;
            yield return new WaitForSeconds(time);
            light.range = 50;
        }

        switchOffLightsTotal();
    }

    private void switchOffLightsTotal()
    {
        foreach (Light light in lightsToSwitchOff)
        {
            light.range = 0;
        }
    }

    private void switchOnFlashlight()
    {
        foreach (GameObject light in lightsToSwitchFlishlight)
        {
            light.GetComponent<Animator>().enabled = true;
            light.GetComponent<AudioSource>().Play();
        }
    }

}
