using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class ShowControls : MonoBehaviour
{
    public bool bShowController = false;
    private bool bShowAnterior = false;
    void Update()
    {
        if (bShowAnterior != bShowController)
        {
            foreach (Hand hand in this.GetComponent<Player>().hands)
            {
                if (bShowController)
                {
                    hand.ShowController();
                    hand.SetSkeletonRangeOfMotion(Valve.VR.EVRSkeletalMotionRange.WithController);
                }
                else
                {
                    hand.HideController();
                    hand.SetSkeletonRangeOfMotion(Valve.VR.EVRSkeletalMotionRange.WithoutController);
                }
            }
        }
        bShowAnterior = bShowController;
    }
}
