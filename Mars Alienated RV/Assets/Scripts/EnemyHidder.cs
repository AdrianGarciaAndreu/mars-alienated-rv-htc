using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHidder : MonoBehaviour
{

    public GameObject EnemyToHide;

    void FixedUpdate()
    {
        GameManager gm = GameManager.Instance;
        if (gm.isInScapeRoom) { 
            RaycastHit hit;
            if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, Mathf.Infinity))
            {
                Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);

                if (hit.collider.gameObject.name == "EnemyDoorChecker")
                {
                    gm.isScapeRoomDoorChecked = true;
                }
                else if (hit.collider.gameObject.tag == "EnemyHidder") { 
                    EnemyToHide.SetActive(false);

                    if (gm.isScapeRoomDoorChecked) {  // Just ends game when the door is looked
                     gm.gameOver = true; // GAME ENDS HERE
                    }
                }
                else
                {
                    if (!EnemyToHide.activeSelf) { EnemyToHide.SetActive(true); }
                }


            }else{
                Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 1000, Color.white);
                if (!EnemyToHide.activeSelf) { EnemyToHide.SetActive(true); }
            }
        }   // End to check if player is in scape room
    }


}
