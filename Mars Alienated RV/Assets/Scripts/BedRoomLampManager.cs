using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BedRoomLampManager : MonoBehaviour
{

    public bool isLampOn = false;
    public GameObject lampLight;
    public GameObject bedRoomDecal;
    


    public void changeLampStatus(){
        if (isLampOn){ // Turns Off Lamp
            isLampOn = false;
            Debug.Log("Lamp Off!");

            lampLight.SetActive(false);
            bedRoomDecal.SetActive(false);

        } else { // Turns On Lamp
            isLampOn = true;
            Debug.Log("Lamp On!");

            lampLight.SetActive(true);
            bedRoomDecal.SetActive(true);
        }
    }



}
