using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class spawnPointRegisterEvents : MonoBehaviour
{


    public UnityEvent<Collider> onTriggerStay;
    public UnityEvent<Collider> onTriggerExit;


    private void OnTriggerStay(Collider col)
    {
        if(col.gameObject.tag != "Ground" && col.gameObject.tag != "Enemy" && col.gameObject.tag != "Player") { 
            if (onTriggerStay != null) onTriggerStay.Invoke(col); // When there is a trigger, is invoked the collider
        }
    }

    private void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag != "Ground" && col.gameObject.tag != "Enemy" && col.gameObject.tag != "Player"){
            if (onTriggerExit != null) onTriggerExit.Invoke(col); // When there is a trigger, is invoked the collider
        }
    }

}
