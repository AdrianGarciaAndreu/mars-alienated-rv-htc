using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayLogo : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Animation anim = this.gameObject.GetComponent<Animation>();
        anim.Play("TitleRaise");


        StartCoroutine(exitGame());
    }

  



    private IEnumerator exitGame()
    {
        for(int i=0; i<15; i++){

            yield return new WaitForSeconds(1);
        }

        Debug.Log("EXIT!");
        Application.Quit();
    }

}
