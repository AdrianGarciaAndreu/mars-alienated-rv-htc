using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sky : MonoBehaviour
{

    public Transform followTarget; // position of actor
    Vector3 offset; // position inicial
    public GameObject star; // star prefab
    public int numberStar;
    public float skyAlt;
    public float maxDistance;

    // Start is called before the first frame update
    void Start()
    {
        offset = followTarget.transform.position;
        GenerateSky();
        
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        transform.position = followTarget.position + offset;
    }

    void GenerateSky()
    {
        Vector3 pos;
        pos.y = skyAlt;

        for(int i =0; i < numberStar; i++)
        {
            pos = Random.onUnitSphere * maxDistance;
            if (pos.y < 0)
            {
                pos.y = -pos.y;
            }
            
            GameObject s = Instantiate(star, pos, star.transform.rotation);
            s.transform.parent = transform;
            s.transform.LookAt(followTarget);
        }
    }
}
