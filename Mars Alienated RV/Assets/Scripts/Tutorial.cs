using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tutorial : MonoBehaviour
{
    public GameObject canvas;
    public Text canvasText;
    public List<GameObject> arrows;
    public List<GameObject> codePanel;
    public List<GameObject> ipad;

    private bool isFirstTimeExitZoom = true;
    private bool isFirstTimeUseZoomState = true;
    private bool isFirstTimeFoundIpad = true;
    private bool isFirstTimeEnd = true;

    private Coroutine actualCoroutine;

    private enum State {
            Init, 
            Move, 
            UseZoom, 
            ExitZoom, 
            FoundIpad,
            End
    };

    private State actualState = State.Init;

    // Start is called before the first frame update
    void Start()
    {
        disableAllObjects();
        showInitText();
    }

    // Update is called once per frame
    void Update()
    {
        switch (actualState)
        {
            case State.Init:
                checkToStartTutorial();
                break;
            case State.Move:
                break;
            case State.UseZoom:
                break;
            case State.ExitZoom:
                break;
        }
    }

    //--------------------------------------------------------------------------------
    //                          INIT STATE
    //--------------------------------------------------------------------------------

    void checkToStartTutorial()
    {
        if (Input.GetButtonDown("A"))
        {
            startMovementState();
        }
    }

    void showInitText()
    {
        this.canvas.SetActive(true);
        actualCoroutine = StartCoroutine(showText("Bienvenido al tutorial de Mars: Alienated. Pulsa A para empezar."));
    }

    void disableAllObjects()
    {
        changeItemsLayer(0, arrows);
        changeItemsLayer(0, codePanel);
        changeItemsLayer(0, ipad);
    }

    //--------------------------------------------------------------------------------
    //                          MOVE STATE
    //--------------------------------------------------------------------------------

    void startMovementState()
    {
        actualState = State.Move;
        changeItemsLayer(8, arrows);
        StopCoroutine(actualCoroutine);
        actualCoroutine = StartCoroutine(showText("Para poder moverte por la base apunta a una marca de movimiento colocada en la base de tus pies, mirando hacia ella y pulsa el bot�n A."));
    }

    //--------------------------------------------------------------------------------
    //                          USE ZOOM STATE
    //--------------------------------------------------------------------------------

    public void startUseZoomState()
    {
        if (isFirstTimeUseZoomState)
        {
            isFirstTimeUseZoomState = false; 
            actualState = State.UseZoom;
            changeItemsLayer(8, codePanel);
            StopCoroutine(actualCoroutine);
            actualCoroutine = StartCoroutine(showText("Fant�stico, ya sabes desplazarte. Para poder avanzar necesitaras interactuar con los objetos de las salas. Ah� hay un panel para abrir la puerta. Apunta a �l y pulsa A."));
        }
    }

    //--------------------------------------------------------------------------------
    //                          EXIT ZOOM STATE
    //--------------------------------------------------------------------------------

    public void startExitZoomState()
    {
        if (isFirstTimeExitZoom)
        {
            isFirstTimeExitZoom = false;
            actualState = State.ExitZoom;
            StopCoroutine(actualCoroutine);
            actualCoroutine = StartCoroutine(showText("Ups, no te acuerdas de la contrase�a, pero recuerdas que Robert podr�a haberlo anotado en alg�n lado. Pulsa B para salir del zoom y busca el n�mero."));
        }
    }

    //--------------------------------------------------------------------------------
    //                          FOUND IPAD STATE
    //--------------------------------------------------------------------------------

    public void startFoundIpadState()
    {
        if (isFirstTimeFoundIpad)
        {
            isFirstTimeFoundIpad = false;
            actualState = State.FoundIpad;
            changeItemsLayer(8, ipad);
            StopCoroutine(actualCoroutine);
            actualCoroutine = StartCoroutine(showText("Busca la contrase�a por la sala e introducela en el panel de acceso para poder abrir la puerta."));
        }
    }

    //--------------------------------------------------------------------------------
    //                          FOUND IPAD STATE
    //--------------------------------------------------------------------------------

    public void startEndState()
    {
        if (isFirstTimeEnd)
        {
            isFirstTimeEnd = false;
            actualState = State.FoundIpad;
            changeItemsLayer(8, ipad);
            StopCoroutine(actualCoroutine);
            actualCoroutine = StartCoroutine(showText("Enhorabuena, has superado el tutorial con �xito. Ya puedes descubrir que le ha sucedido a tu compa�ero."));

            StartCoroutine(unshowCanvas());
        }
    }

    IEnumerator unshowCanvas()
    {
        yield return new WaitForSeconds(5);
        canvas.SetActive(false);
    }

    //--------------------------------------------------------------------------------
    //                          OTHERS
    //--------------------------------------------------------------------------------

    void changeItemsLayer(int layer, List<GameObject> list)
    {
        foreach(GameObject it in list)
        {
            it.layer = layer;
        }
    }

    IEnumerator showText(string text)
    {
        canvasText.text = "";
        float time = text.Length * 0.0003f;
        for (int i = 0; i < text.Length; i++)
        {
            canvasText.text += text[i];
            yield return new WaitForSeconds(time);
        }
    }
}
