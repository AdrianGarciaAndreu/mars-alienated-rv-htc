using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class greenhousePotManager : MonoBehaviour
{

    private GameManager gm;

    public int potBaseIndex = -1; // Index for the puzzle 
    public bool potPlaced = false;
    
    public Vector3 potPlacedOriginalTransform;
    potPlacedColor _potPlacedColor;

    
    private void Start(){
        gm = GameManager.Instance;

    }


    private void OnTriggerEnter(Collider other)
    {
       // Debug.Log(other.gameObject.tag);
        if (!potPlaced) { // If there is no pot placed at base yet

            potPlaced = true; // flag to know where a pot is placed already

            // checks what color is the pot placed at the base
            GameObject localPot = null;
            if (other.gameObject.tag == "PlantRed"){ localPot = gameObject.transform.Find("PlantRed").gameObject; _potPlacedColor = potPlacedColor.Red; }
            if (other.gameObject.tag == "PlantGreen") { localPot = gameObject.transform.Find("PlantGreen").gameObject; _potPlacedColor = potPlacedColor.Green; }
            if (other.gameObject.tag == "PlantBlue"){localPot = gameObject.transform.Find("PlantBlue").gameObject; _potPlacedColor = potPlacedColor.Blue; }

            if (localPot != null) { 
                localPot.SetActive(true);
            
                // Saves the pot original position
                potPlacedOriginalTransform.x = other.gameObject.GetComponent<PlantManager>().originalTransformation.x;
                potPlacedOriginalTransform.y = other.gameObject.GetComponent<PlantManager>().originalTransformation.y;
                potPlacedOriginalTransform.z = other.gameObject.GetComponent<PlantManager>().originalTransformation.z;

                // Report to gameManager and Destroy the original pot
                gm.addPotPlacedToList(potBaseIndex, _potPlacedColor, potPlacedOriginalTransform); // Pass puzzle info the GameManager
                Destroy(other.transform.parent.gameObject);

                
                Debug.Log("Pot " + _potPlacedColor + " placed at" + potPlacedOriginalTransform);

            }
        } 

    }




}
