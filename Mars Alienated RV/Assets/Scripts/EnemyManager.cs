using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    // Variables
    #region

    /////////////////////////////////////////////////
    ////////// Enemy and Spawn points ///////////////
    /////////////////////////////////////////////////
    public GameObject enemy;
    public GameObject[] spawnPoints;
    //public spawnPointRegisterEvents[] spawnPointRegisterEventList;
    //private ArrayList possibleSpawnPoints = new ArrayList();

    private bool enemyInGame = false; // Enemy in game FLAG
   
    /*
    private bool spawnPointFront_Allowed = true; // Spawn points available to move FLAGS
    private bool spawnPointBack_Allowed = true;
    private bool spawnPointLeft_Allowed = true;
    private bool spawnPointRight_Allowed = true;
    */

    /////////////////////////////////////////////////
    ////////// Distances and times //////////////////
    /////////////////////////////////////////////////
    //private float minDistance = 3f;
    //private float incrementForRange = 0.5f;

    //public float __totalTime = 1000.0f; // Total GAME TIME 
    //private float currentTime;
    public float timeInterval = 15.0f;   // Time interval in seconds for each enemy appearance
    private float currentTimeInterval;

    #endregion

    /*

    private void OnEnable()
    {

        // Register events for the spawn pointns trigger stay detection
        spawnPointRegisterEventList[0].onTriggerStay.AddListener(spawnPointTriggerStayFront);
        spawnPointRegisterEventList[1].onTriggerStay.AddListener(spawnPointTriggerStayBack);
        spawnPointRegisterEventList[2].onTriggerStay.AddListener(spawnPointTriggerStayLeft);
        spawnPointRegisterEventList[3].onTriggerStay.AddListener(spawnPointTriggerStayRight);

        spawnPointRegisterEventList[0].onTriggerExit.AddListener(spawnPointTriggerExitFront);
        spawnPointRegisterEventList[1].onTriggerExit.AddListener(spawnPointTriggerExitBack);
        spawnPointRegisterEventList[2].onTriggerExit.AddListener(spawnPointTriggerExitLeft);
        spawnPointRegisterEventList[3].onTriggerExit.AddListener(spawnPointTriggerExitRight);

    }
    */

    // Start is called before the first frame update
    void Start() {
        //currentTime = __totalTime;
        currentTimeInterval = timeInterval;

        StartCoroutine(customLoop()); }


    void FixedUpdate()
    {

        LayerMask lm = 1 << 9;
        //lm = ~lm;

        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, Mathf.Infinity, lm))
        {
            GameObject target = hit.collider.gameObject;
            if(target.tag == "Enemy") {  // Destroys the enemy
                Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.red);
                target.GetComponent<Enemy>().delayedDestroy(0.25f);
            }
        }
        else
        {
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 1000, Color.yellow);
            //Debug.Log("Did not Hit");
        }

    }




        /*
         * Enemy Manager custom loop
         * Decrements game time
         */
        private IEnumerator customLoop()
    {
        while (true) {
            ReduceTime();
            //ReduceRange(); // DEBUG: Just to debug range decrements
            yield return new WaitForSeconds(1.0f); // Each second
        }

    }

    /*
     * Decrements game time and controls enemy appears
     */
    private void ReduceTime() {
        if (currentTimeInterval > 0) { currentTimeInterval--; } // Decrements time for enemy appears
        else {InstantiateEnemy();}

    }



    private void InstantiateEnemy()
    {
        if (!enemyInGame)
        {

            // Get random point
            if (spawnPoints.Length > 0) { 
                int selectedIndex = Random.Range(0, spawnPoints.Length);
                
                enemyInGame = true;
                GameObject enemy = Instantiate(this.enemy, spawnPoints[selectedIndex].transform.position, spawnPoints[selectedIndex].transform.rotation);
                Debug.Log("Enemy instantiate!");
            }
        } // End Enemy is in game

    }



    /*
     *  Reduces the enemy spawn range arround the player
     */
    /*
  private void ReduceRange()
  {
      GameObject sp;
      Vector3 sp_pos;

      // FRONT POINT
      sp = spawnPoints[0];
      sp_pos = sp.transform.localPosition;
      if (sp_pos.z >= minDistance) {
          sp_pos.z = sp_pos.z - incrementForRange;
          sp.transform.localPosition = sp_pos;
      }



      // BACK POINT
      sp = spawnPoints[1];
      sp_pos = sp.transform.localPosition;
      if (sp_pos.z <= -minDistance)
      {
          sp_pos.z = sp_pos.z + incrementForRange;
          sp.transform.localPosition = sp_pos;
      }


      // LEFT POINT
      sp = spawnPoints[2];
      sp_pos = sp.transform.localPosition;
      if (sp_pos.x <= -minDistance)
      {
          sp_pos.x = sp_pos.x + incrementForRange;
          sp.transform.localPosition = sp_pos;
      }


      // RIGHT POINT
      sp = spawnPoints[3];
      sp_pos = sp.transform.localPosition;
      if (sp_pos.x >= minDistance)
      {
          sp_pos.x = sp_pos.x - incrementForRange;
          sp.transform.localPosition = sp_pos;
      }


  }
    */


  /*
   *  Instantiate the enemy in a random possible spawn point
   */
  /*
    private void InstantiateEnemy()
    {
       // if (!enemyInGame) { 
            
           possibleSpawnPoints.Clear(); //Build array of possible spawn points from existing ones

            if (spawnPointFront_Allowed) { possibleSpawnPoints.Add(spawnPoints[0]); }
            if (spawnPointBack_Allowed) { possibleSpawnPoints.Add(spawnPoints[1]); }
            if (spawnPointLeft_Allowed) { possibleSpawnPoints.Add(spawnPoints[2]); }
            if (spawnPointRight_Allowed) { possibleSpawnPoints.Add(spawnPoints[3]); }


            if (possibleSpawnPoints.Count > 0) {
                enemyInGame = true;
                int selectedIndex = Random.Range(0, possibleSpawnPoints.Count);
                //Debug.Log("SELECTED INDEX: " + selectedIndex);

                GameObject spawnPoint = (GameObject)possibleSpawnPoints[selectedIndex];
                GameObject enemy = Instantiate(this.enemy, spawnPoint.transform.position, Quaternion.identity);
                enemy.transform.LookAt(this.gameObject.transform);
                this.currentTimeInterval = timeInterval;
                ReduceRange();
            } // Ends check possible spawn points count
            else
            {
                this.currentTimeInterval = timeInterval;
                ReduceRange();
            }
        }
    }
  */

    
    /*
     *  Set if the enemy is instantiated
     */
    public void SetEnemyInGame(bool exists)
    {
        this.enemyInGame = exists;
    }

    /*

    ///////////////////////////////////////
    // Event registers for trigger stay //
    //////////////////////////////////////
    #region

    private void spawnPointTriggerStayFront(Collider col){
        //Debug.Log("Front with Collisions");
        spawnPointFront_Allowed = false;
    }

    private void spawnPointTriggerStayBack(Collider col)
    {
        //Debug.Log("Back with Collisions");
        spawnPointBack_Allowed = false;

    }
    private void spawnPointTriggerStayRight(Collider col)
    {
        //Debug.Log("Right with Collisions");
        spawnPointRight_Allowed = false;

    }
    private void spawnPointTriggerStayLeft(Collider col)
    {
        //Debug.Log("Left with Collisions");
        spawnPointLeft_Allowed = false;

    }

    #endregion


    ///////////////////////////////////////
    // Event registers for trigger exit //
    //////////////////////////////////////
    #region

    private void spawnPointTriggerExitFront(Collider col)
    {
        spawnPointFront_Allowed = true;
    }

    private void spawnPointTriggerExitBack(Collider col)
    {
        //Debug.Log("Back with Collisions");
        spawnPointBack_Allowed = true;

    }
    private void spawnPointTriggerExitRight(Collider col)
    {
        Debug.Log("Right withOUT Collisions");
        spawnPointRight_Allowed = true;

    }
    private void spawnPointTriggerExitLeft(Collider col)
    {
        //Debug.Log("Left with Collisions");
        spawnPointLeft_Allowed = true;

    }
    #endregion // Exit triggers
    */

}

