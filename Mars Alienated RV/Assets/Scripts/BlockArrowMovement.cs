using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockArrowMovement : MonoBehaviour
{
    private const float _maxDistance = 1.2f;

    void Update()
    {
        RaycastHit hit;
        Debug.DrawLine(transform.position, transform.position + transform.forward * _maxDistance, Color.green);
        if (Physics.Raycast(transform.position, transform.forward, out hit, _maxDistance))
        {
            // GameObject detected in front of the camera.
            setActiveArrow(false);
        }
        else
        {
            setActiveArrow(true);
        }
    }

    void setActiveArrow(bool isActive)
    {
        var gameObject = this.gameObject.transform.parent.gameObject.transform;
        gameObject.GetChild(0).gameObject.SetActive(isActive);
    }
}
