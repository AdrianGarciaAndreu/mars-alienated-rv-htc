using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuScene : MonoBehaviour
{
    
    public string sceneToChange;
    public GameObject transitionObject;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ExitGame(){
        Application.Quit();
    }

    public void PlayGame(){
        Debug.Log("Video Over");
        Animation fadeOut = transitionObject.GetComponent<Animation>();

        fadeOut.Play("FadeOut");

        //SceneManager.LoadScene(sceneToChange);
        StartCoroutine(MakeTheLoad(sceneToChange));
    }

    
    // Tieme to load
    IEnumerator MakeTheLoad(string scene)
    {
        yield return new WaitForSeconds(1f);
        AsyncOperation operation = SceneManager.LoadSceneAsync(scene);

        while (operation.isDone == false)
        {
            yield return null;
        }
    }

}
