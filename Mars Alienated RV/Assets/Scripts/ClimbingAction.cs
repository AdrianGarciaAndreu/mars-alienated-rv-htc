using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class ClimbingAction : MonoBehaviour
{
    public SteamVR_Action_Boolean pulsaGatillo;
    public bool[] bInGrip; //indica si la mano est� dentro del agarrador
    private bool[] bTrigger; //indica si se ha pulsado el gatillo
    private Vector3[] prevPos;
    private void Start()
    {
        bTrigger = new bool[this.GetComponent<Player>().hands.Length];
        bInGrip = new bool[this.GetComponent<Player>().hands.Length];
        prevPos = new Vector3[this.GetComponent<Player>().hands.Length];
    }
    void Update()
    {
        for (int i = 0; i < this.GetComponent<Player>().hands.Length; i++)
        {
            if (pulsaGatillo[this.GetComponent<Player>().hands[i].handType].stateDown)
            {
                bTrigger[i] = true;
            }
            if (pulsaGatillo[this.GetComponent<Player>().hands[i].handType].stateUp)
            {
                bTrigger[i] = false;
            }
        }
        if ((bInGrip[0] && bTrigger[0]) || (bInGrip[1] && bTrigger[1]))
        {
            this.GetComponent<Rigidbody>().useGravity = false;
            this.GetComponent<Rigidbody>().isKinematic = true;
        }
        else
        {
            this.GetComponent<Rigidbody>().useGravity = true;
            this.GetComponent<Rigidbody>().isKinematic = false;
        }

        for (int i = 0; i < this.GetComponent<Player>().hands.Length; i++)
        {
            if (pulsaGatillo[this.GetComponent<Player>().hands[i].handType].stateDown)
            {
                bTrigger[i] = true;
            }
            if (pulsaGatillo[this.GetComponent<Player>().hands[i].handType].stateUp)
            {
                bTrigger[i] = false;
            }
            if (bInGrip[i] && bTrigger[i])
            {
                transform.Translate(prevPos[i] - this.GetComponent<Player>().hands[i].transform.localPosition);
            }
            prevPos[i] = this.GetComponent<Player>().hands[i].transform.localPosition;
        }
    }
}
