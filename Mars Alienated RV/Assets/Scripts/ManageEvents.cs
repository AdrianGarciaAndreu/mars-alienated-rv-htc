using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.Extras;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class ManageEvents : MonoBehaviour
{
    //public SteamVR_Action_Boolean pulsaR1;
    private GameObject other;
    private bool bDragging;
    //private drag dragObj;
    private float distRay;

    public Hand rightHand;
    public SteamVR_Action_Boolean buttonA;
    public GameObject eyehole;

    private GameObject _gazedAtObject = null;
    private bool isClickable = false;

    void Awake()
    {
        rightHand.GetComponent<SteamVR_LaserPointer>().PointerIn += PointerInside;
        rightHand.GetComponent<SteamVR_LaserPointer>().PointerOut += PointerOutside;
    }

    public void PointerInside(object sender, PointerEventArgs e)
    {
        other = e.target.gameObject;
        CheckInteractableObjects();
    }

    public void PointerOutside(object sender, PointerEventArgs e)
    {
        other = null;
        isClickable = false;
        changePointerImage(isClickable);
    }

    public void CheckInteractableObjects()
    {
        if (LayerMask.LayerToName(other.layer) == "receiver")
        {
            isClickable = true;
            changePointerImage(isClickable);
        }
    }

    void changePointerImage(bool state)
    {
        eyehole.GetComponent<Animator>().SetBool("Focus", state);
    }

    private void Update()
    {
        if (buttonA[rightHand.handType].stateDown && isClickable)
        {
            // New GameObject.
            _gazedAtObject?.SendMessage("OnPointerExit");
            _gazedAtObject = other;
            _gazedAtObject.SendMessage("OnPointerEnter");
        }
    }
}
