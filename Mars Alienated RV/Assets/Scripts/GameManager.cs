using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


// Static enumerations ///////////
public enum potPlacedColor { Red, Blue, Green }; 


public class GameManager : MonoBehaviour
{
    /*******************************
        Global Game Variables
    ********************************/
    private static GameManager _instance;

    public bool gameOver = false; // is gameEnded?
    public bool gameLost = false; // when game is Over, the player lost?

    public int totalGameTime = 30; // Total game time (timer) in SECONDS
    public int totalGatheredData = 0; // When it cames 100, the player be able to escape (GAME OVER)

    public bool isInScapeRoom = false;
    public bool isScapeRoomDoorChecked = false; // checks when the user has looked to the scape door or no
    
    
    
    
    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject go = new GameObject("GameManager");
                go.AddComponent<GameManager>();
            }

            return _instance;
        }
    }

    void Awake() {
        _instance = this;
    }

    private void Update(){
        if (gameOver){
            StartCoroutine(LoadEnd(5));
        }
    }





    /*******************************
            Beedroom Puzzles 
    ********************************/
    public bool lampIsOn = false;



    /*******************************
            Greenhouse Puzzles 
    ********************************/
    #region

    public GameObject potRed, potBlue, potGreen;

    public potPlacedColor[] potCorrectListOrder = new potPlacedColor[3];

    private potPlacedColor[] potPlacedColorList = new potPlacedColor[3];
    private Vector3[] potPlacedTransformList = new Vector3[3];

    private bool pot1Placed = false, pot2Placed = false, pot3Placed = false;



    // Adds to the placed pot list a pot color in a List Index
    public void addPotPlacedToList(int index, potPlacedColor potColor, Vector3 v3){
        
        potPlacedColorList[index] = potColor;
        potPlacedTransformList[index] = v3;

        switch (index){
            case 0:
                pot1Placed = true;
                break;
            case 1:
                pot2Placed = true;
                break;
            case 2:
                pot3Placed = true;
                break;
            
            default:
                // Do nothing
                break;
        } // end switch

        if(pot1Placed && pot2Placed && pot3Placed) { checkPotGameIsCompleted(); }

    }



    private void checkPotGameIsCompleted() {

        // Compare list with puzzle result list
        Debug.Log("Game completed!!!");

        bool differentOorder = false;
        for (int i = 0; i < potPlacedColorList.Length; i++){
            if(potPlacedColorList[i] != potCorrectListOrder[i]) { differentOorder = true; }
        }

        if (differentOorder) { resetPotGame(); }
        else { 

            ////// Greenhouse pot puzzle solved !!
            totalGatheredData += 25;
        }
    }



    private void resetPotGame() {

        // Clear pots
        pot1Placed = false; pot2Placed = false; pot3Placed = false;

        foreach (GameObject go in GameObject.FindGameObjectsWithTag("PlantBase")){
            go.GetComponent<greenhousePotManager>().potPlaced = false;
            go.transform.Find("PlantRed").gameObject.SetActive(false);
            go.transform.Find("PlantGreen").gameObject.SetActive(false);
            go.transform.Find("PlantBlue").gameObject.SetActive(false);
        }

        // Reset pots original position and color
        for (int i = 0; i < potPlacedColorList.Length; i++)
        {
            Debug.Log("Entering loop " + i);
            GameObject potToInstantiate = null;
            if (potPlacedColorList[i] == potPlacedColor.Red) { potToInstantiate = potRed; }
            if (potPlacedColorList[i] == potPlacedColor.Blue) { potToInstantiate = potBlue; }
            if (potPlacedColorList[i] == potPlacedColor.Green) { potToInstantiate = potGreen; }

            GameObject pot = Instantiate(potToInstantiate, potPlacedTransformList[i], Quaternion.identity);

        }

        potPlacedColorList = new potPlacedColor[3];
        potPlacedTransformList = new Vector3[3];

    }

    #endregion





    IEnumerator LoadEnd(int secs)
    {

        // Wait some seconds
        for(int i=0; i<secs; i++){
            Debug.Log("Waitting for game over, second " + i);
            yield return new WaitForSeconds(1f);
        }


        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("SceneEnd");

        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }


}

