using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenTargetRoom : MonoBehaviour
{
    public OpenCloseDoor door;

    private bool isDoorOpen = false;

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.name == "tarjet" && !isDoorOpen)
        {
            door.openDoor();
            isDoorOpen = true;
        }
    }
}
