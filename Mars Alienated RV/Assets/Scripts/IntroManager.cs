using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class IntroManager : MonoBehaviour
{

    public string sceneToChange;
    public GameObject transitionObject;
    private UnityEngine.Video.VideoPlayer videoPlayer;

    // Start is called before the first frame update
    void Start(){

        videoPlayer = gameObject.GetComponent<UnityEngine.Video.VideoPlayer>();
        videoPlayer.loopPointReached += onVideoEnd;
        Debug.Log("Video events...");


    }


    void onVideoEnd(UnityEngine.Video.VideoPlayer vp)
    {
        Debug.Log("Video Over");
        Animation fadeOut = transitionObject.GetComponent<Animation>();

        fadeOut.Play("FadeOut");

        //SceneManager.LoadScene(sceneToChange);
        StartCoroutine(MakeTheLoad(sceneToChange));

    }



    // Tieme to load
    IEnumerator MakeTheLoad(string scene)
    {
        yield return new WaitForSeconds(1f);
        AsyncOperation operation = SceneManager.LoadSceneAsync(scene);

        while (operation.isDone == false)
        {
            yield return null;
        }
    }


}
