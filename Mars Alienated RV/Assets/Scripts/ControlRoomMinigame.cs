using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class ControlRoomMinigame : MonoBehaviour
{
    public List<LineRenderer> lines = new List<LineRenderer>();

    public List<GameObject> objects = new List<GameObject>();

    public Material matOriginal = null;
    public Material matSelected = null;

    private GameObject selected = null;
    private GameObject lastSelected = null;

    private List<bool> usedLines = new List<bool>();
    private List<bool> usedObjects = new List<bool>();

    private int totalCorrect = 0;

    private GameManager gm;

    /**
     * Resetea todos los parametros
     **/
    void Start()
    {
        this.gm = GameManager.Instance;

        resetLines();
        resetUsedObjects();
        resetAllObjects();
        totalCorrect = 0;
    }

    /**
     * Se ejecuta al hacer click en A cuando el puntero est� sobre un cubo del minijuego
     **/
    public void selectOption(GameObject obj)
    {
        //Cambia el estado del objeto para saber que ese ya ha sido seleccionado o viceversa
        changeObjectState(obj);

        //Se comprueban 3 casos:
        //1. Sea el primer objeto de la pareja en ser seleccionado
        //1.1 Sea el mismo objeto (se deselecciona y cambia de material)
        //1.2 Sea distinto objeto (se selecciona y cambia de material)
        //2. Sea el segundo objeto de la pareja en ser seleccionado (se dibuja la linea)
        if (selected == null && lastSelected == null)
        {
            selected = obj;
            changeMaterial(matSelected);
        }
        else if(obj != selected && lastSelected == null)
        {
            changeMaterial(matOriginal);

            lastSelected = selected;
            selected = obj;

            drawLine();

            lastSelected = null;
            selected = null;
        }
        else if(obj == selected)
        {
            changeMaterial(matOriginal);
            selected = null;
        }
    }

    /**
     * Cambia el estado de un objeto de usado a no usado y viceversa
     **/
    private void changeObjectState(GameObject obj)
    {
        int index = 0;
        for (int i = 0; i < objects.Count; i++)
        {
            if (objects[i] == obj)
            {
                index = i;
            }
        }

        usedObjects[index] = !usedObjects[index];
    }

    /**
     * Dibuja una linea entre la posici�n de dos objetos, posteriormente, 
     * comprueba si la pareja es correcta 
     * y por �ltimo en caso de que esten todas las lineas dibujadas, comprueba si se ha ganado o perdido
     **/
    private void drawLine()
    {
        for (int i = 0; i < usedLines.Count; i++)
        {
            if(usedLines[i] == false)
            {
                var points = new Vector3[2];
                points[0] = lastSelected.transform.localPosition;
                points[1] = selected.transform.localPosition;

                lines[i].positionCount = 2;
                lines[i].SetPositions(points);
                usedLines[i] = true;

                if(lastSelected.name == selected.name)
                {
                    totalCorrect++;
                }

                lastSelected.layer = 0;
                selected.layer = 0;

                if (ifAllLinesAreDrawed())
                {
                    checkIfGameIsWin();
                }

                return;
            }
        }
    }

    /**
     * Comprueba si todas las lineas han sido dibujadas
     **/
    private bool ifAllLinesAreDrawed()
    {
        bool isAllDrawed = true;

        for (int i = 0; i < usedLines.Count; i++)
        {
            if (usedLines[i] == false)
            {
                isAllDrawed = false;
            }
        }

        return isAllDrawed;
    }

    /**
     * Comprueba si se ha ganado, sino, resetea todos los par�metros
     **/
    private void checkIfGameIsWin()
    {
        if(totalCorrect == lines.Count)
        {
            Debug.Log("HAS GANADO");
            gm.totalGatheredData += 25;
        }
        else
        {
            resetLines();
            resetUsedObjects();
            resetAllObjects();
            totalCorrect = 0;
        }
    }

    /**
     * Cambia el material del objeto seleccionado
     **/
    private void changeMaterial(Material mat)
    {
        selected.GetComponent<MeshRenderer>().material = mat;
    }

    /**
     * Resetea todas las lineas (elimina los puntos que las formaban),
     * pone todas las lineas como no usadas y los objetos seleccionados a null
     **/
    private void resetLines()
    {
        usedLines = new List<bool>();

        for (int i = 0; i < lines.Count; i++)
        {
            lines[i].positionCount = 0;
            usedLines.Add(false);
        }

        selected = null;
        lastSelected = null;
    }

    /**
     * Pone todos los objetos como no utilizadas
     **/
    private void resetUsedObjects()
    {
        usedObjects = new List<bool>();

        for (int i = 0; i < objects.Count; i++)
        {
            usedObjects.Add(false);
        }
    }

    /**
     * Resetea todos los objetos, poniendoles el material original 
     * y activando el layer para que se pueda volver a interaccionar sobre ellos
     **/
    private void resetAllObjects()
    {
        for (int i = 0; i < objects.Count; i++)
        {
            objects[i].GetComponent<MeshRenderer>().material = matOriginal;
            objects[i].layer = LayerMask.NameToLayer("receiver");
        }
    }
}
