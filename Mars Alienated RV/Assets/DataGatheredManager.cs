using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DataGatheredManager : MonoBehaviour
{


    GameManager gm;
    private int lastQtyDataGathered;
    private int currentDataGathered;

    void Start()
    {
        this.gm = GameManager.Instance;
        this.lastQtyDataGathered = -1;
    }
    
    void Update() {
        collectDataGatheredFromGM();
    }


    private void collectDataGatheredFromGM(){
        this.currentDataGathered = gm.totalGatheredData;
        if (this.currentDataGathered != this.lastQtyDataGathered) { 
            TextMeshPro tmp = this.gameObject.GetComponent<TextMeshPro>();
            tmp.SetText("Datos obtenidos\n"+currentDataGathered + "%");

            this.lastQtyDataGathered = this.currentDataGathered;
        }

        
    }
}
