using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveCollisionElevator : MonoBehaviour
{
    public GameObject player;

    private bool isFirstCollision = true;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (isFirstCollision)
        {
            isFirstCollision = false;
            player.GetComponent<Rigidbody>().isKinematic = false;
            player.GetComponent<Rigidbody>().freezeRotation = true;
            player.GetComponent<BoxCollider>().enabled = true;
        }
    }
}
